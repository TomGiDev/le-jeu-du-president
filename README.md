# Le jeu du président
Le président (aussi appelé le troufion) est un jeu de cartes rapide et amusant, au cours duquel la hiérarchie des joueurs changera à chaque manche. Le vainqueur d'une manche devient le président, alors que le perdant est proclamé troufion. Une fois que vous maitriserez les règles de base, vous pourrez essayer différentes variantes de ce jeu très populaire.
Règles du jeu :

- Ce jeu se joue de 3 à 6 joueurs.
- Lors du premier tour, le joueur possédant la dame de coeur commence.
- L'ensemble des cartes sont distribuées aux joueurs de la manière la plus homogène.
- Ce jeu se joue par tours. Tant que quelqu'un peut et veut jouer, le tour continue et tourne dans le sens horaire.
- Le premier joueur choisit des cartes d'une même valeur et les pose sur la tables.
- Suite à celà, chaque joueur doit fournir autant de cartes que le joueur précédent des cartes dun' valeur supérieure ou égale.
- Un joueur a le droit de sauter son tour et reprendre le tour d'après.
- Un tour est fini lorsque plus personne ne joue. C'est alors le dernier à avoir joué qui ouvre la manche suivante. Ou si un joueur pose un ou plusieurs deux. Ce qui mets immédiatement fin au tour.
- L'objectif est d'être le premier à ne plus avoir de cartes. Ce joueur est alors déclaré président de la manche.
- Les joueurs restants continuent à jouer jusqu'à ce qu'il n'y ait plus qu'une joueur qui ait des cartes en main, il est alors déclaré 'troufion'

On décide alors ou non de jouer une nouvelle manche. Ce sera le troufion qui ouvrira la partie.


## Lancement du jeu
Veuillez cloner le dossier sur votre environnement :
```
$ git clone https://gitlab.com/TomGiDev/le-jeu-du-president.git
```

Une fois dans le repertoire du jeu:
```
$ cd le-jeu-du-president
```

Merci d'importer les librairies obligatoires nécessaires au bon fonctionnement du jeu dans le fichier `requirements.txt`.

Puis faire un `Run` du fichier `main.py`.

Lors du lancement des instructions / règles seront rappelées brièvement dans l'application.

## Bon jeu !