"""Import the library"""
import random
import names


class Card:
    """
    The class Card allows you to create cards which will be used to create a new Deck to play President Game cards.
    Create a new card with 2 mains parameters (height, color) and 1 parameter
    to compare cards between each other with value.
    :params
        height : height of a card
        color : color present in four different colors
        value : value to compare thr cards with each other
    """

    def __init__(self, height: str, color: str, value: int = 0):
        self.height = height
        self.color = color
        self.value = value

    def __eq__(self, other):
        """
        This function allows you to compare the equality between two cards in order of height
        """

        return self.value == other.value

    def __gt__(self, other):
        """
        This function allows you to compare the superiority between two cards in order of height
        """

        return self.value > other.value

    def __lt__(self, other):
        """
        This function allows you to compare the inferiority between two cards in order of height
        """

        return self.value < other.value

    def __ge__(self, other):
        """
        This function allows you to compare the inferiority or equal between two cards in order of height
        """

        return self.value >= other.value

    def __le__(self, other):
        """
        This function allows you to compare the superiority or equal between two cards in order of height
        """

        return self.value <= other.value

    def __repr__(self):
        """
        This function allows you to check the color and the height of a card.
        """

        return self.height + self.color

    def sort_value(self):
        """
        This function allows you to sort the cards using the key value.
        """

        return self.value


class Deck:
    """
    This class allows you to create a deck for each game with all the four colors '♠', '♥', '♦', '♣'
    and the thirteen different height of cards.
    Create a new deck which is composed with 52 cards and an order according to the values of the cards.
    """
    COLORS = [' ♠', ' ♥', ' ♦', ' ♣']
    HEIGHTS = {'3': 1, '4': 2, '5': 3, '6': 4, '7': 5, '8': 6, '9': 7, '10': 8, 'J': 9,
               'Q': 10, 'K': 11, 'A': 12, '2': 13}

    def __init__(self):
        self.cards = []
        self.create_deck()

    def create_deck(self):
        """
        This function allows you to create a deck of 52 cards.
        """

        for color in Deck.COLORS:
            for height, value in Deck.HEIGHTS.items():
                self.cards.append(Card(height, color, value))

    def shuffle(self):
        """
        This function allows you to shuffle all the cards in a deck
        """

        random.shuffle(self.cards)
        return self.cards


class Player:
    """
    This class allows you to use a player, with a name parameter, who can play this game.
    Create a new player which have a name.
    """

    def __init__(self, name: str = None, role: str = None):
        self.name = name if name is not None else names.get_first_name()
        self.role = role
        self.hand = []

    def add_to_hand(self, card):
        """
        This function allows a player to receive cards from the deck to create a hand of cards.
        """

        return self.hand.append(card)

    def remove_to_hand(self, card):
        """
        This function allows a player to remove cards from the hand of cards.
        """

        return self.hand.remove(card)

    def sort_hand(self):
        """
        This function allows you to sort cards.
        """

        return self.hand.sort(key=Card.sort_value)


class PresidentGame:
    """
    This class allows you to play the game using all different classes before it.
    create a new group of players with their name.
    """

    def __init__(self):
        self.players = []
        self.index = -1
        self.deck = Deck().shuffle()

    def add_players(self, players: list):
        """
        This function allows you to add new players to the game.
        """
        self.players = players
        return self.players

    def next_player(self):
        """
        This function allows you to have the next player.
        """

        self.index = (self.index + 1) % len(self.players)
        return self.players[self.index]

    def distribute_cards(self):
        """
        This function will distribute the cards from a deck to the players.
        """

        deck_shuffle = self.deck
        while len(deck_shuffle) != 0:
            player = self.next_player()
            player.add_to_hand(deck_shuffle.pop())

    def sort_hand_players(self):
        """
        This function allows you to sort the cards in the hand player.
        """

        for player in self.players:
            player.sort_hand()

    def player_have_the_queen_of_hearth(self):
        """
        This function allows you to know which player has the queen of heart.
        """

        start_player = None
        for player in self.players:
            for card in player.hand:
                if card.color == " ♥" and card.height == 'Q':
                    start_player = player
        return start_player

    def player_card_chosen_by_ia(self, start_player: Player):
        """
        This function allows the IA to select automatically 1 to 4 same
        lower cards in its hand and remove them from its hands.
        """

        nb_card_remove = 0
        if start_player.hand[0] < start_player.hand[1]:
            nb_card_remove = 1
        elif start_player.hand[0] == start_player.hand[1] and start_player.hand[1] < start_player.hand[2]:
            nb_card_remove = 2
        elif start_player.hand[0] == start_player.hand[1] and start_player.hand[0] == start_player.hand[2] and \
                start_player.hand[2] < start_player.hand[3]:
            nb_card_remove = 3
        elif start_player.hand[0] == start_player.hand[1] and start_player.hand[0] == start_player.hand[2] and \
                start_player.hand[0] == start_player.hand[3]:
            nb_card_remove = 4

        return nb_card_remove
