import models

play_game = ''
nb_players: int = 0
nb_players_human: int = 0
NB_PLAYERS_MIN: int = 3
NB_PLAYERS_MAX: int = 6
players_name: list = []
name: str
president: str
asshole: str

print(' - - - - - - - - - - - / ♠ / ♥ / ♦ / ♣ / - - - - - - - - - - -')
while play_game not in ['oui', 'non']:
    play_game = str(input('Voulez-vous jouer au jeu de carte : Le Président ? (oui ou non) ').lower())
    if play_game == 'non':
        print('A bientôt')
        print(' - - - - - - - - - - - / ♠ / ♥ / ♦ / ♣ / - - - - - - - - - - -')
        exit()
    elif play_game == 'oui':
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')
        print('Le Jeu du Président peut recevoir de 3 à 6 joueurs.')
        print('Pour pourrez ajouter des IA pour faire une partie à 6 joueurs maximum.')
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')

        while nb_players < NB_PLAYERS_MIN or nb_players > NB_PLAYERS_MAX:
            nb_players = int(input('Combien de joueurs êtes vous pour la partie ? (min : 3 à max : 6) '))
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')
        print('La partie sera constituée de ' + str(nb_players) + ' joueurs.')
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')

        while nb_players_human <= 0 or nb_players_human > nb_players:
            nb_players_human = int(input('Combien d\'humains êtes vous pour la partie ? '))
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')
        print('Enregistrement de vos noms : ')
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')
        i = 1
        while i <= nb_players_human:
            name = str(input('Veuillez rentrer le pseudo du joueur ' + str(i) + ' svp ? '))
            players_name.append(models.Player(name))
            i += 1
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')
        nb_players_ia = nb_players - nb_players_human
        i = 1
        while i <= nb_players_ia:
            players_name.append(models.Player())
            i += 1

        # Display list of players (humans and IA)
        print('Les joueurs enregistrés sont : ')
        for i in range(len(players_name)):
            print('- Joueur ' + str(i + 1) + ' : ' + players_name[i].name)
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')

        print('Début de la partie !')
        print('Dame de coeur à vous l\'honneur !')
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')

        game = models.PresidentGame()
        game.add_players(players_name)
        game.distribute_cards()
        game.sort_hand_players()
        start_player = game.player_have_the_queen_of_hearth()
        index = 0

        start_player_tour = game.players[0]
        print(start_player_tour.name, "à vous de commencer ! ")
        print(start_player.hand)
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')

        # 1er coup
        table_of_game = []
        nb_card_remove = 0
        if start_player.hand[0] < start_player.hand[1]:
            nb_card_remove = 1
        elif start_player.hand[0] == start_player.hand[1] and start_player.hand[1] < start_player.hand[2]:
            nb_card_remove = 2
        elif start_player.hand[0] == start_player.hand[1] and start_player.hand[0] == start_player.hand[2] and \
                start_player.hand[2] < start_player.hand[3]:
            nb_card_remove = 3
        elif start_player.hand[0] == start_player.hand[1] and start_player.hand[0] == start_player.hand[2] and \
                start_player.hand[
                    0] == start_player.hand[3]:
            nb_card_remove = 4

        table_of_game.extend(start_player.hand[0:nb_card_remove])
        del start_player.hand[0:nb_card_remove]
        print("table de jeu : ", table_of_game)
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')

        # 2eme coup
        next_player = game.next_player()
        print("à vous de jouer", next_player.name, next_player.hand)
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')
