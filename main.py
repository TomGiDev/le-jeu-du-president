import models

play_game = ''
nb_players: int = 0
nb_players_human: int = 0
NB_PLAYERS_MIN: int = 3
NB_PLAYERS_MAX: int = 6
players_name: list = []
selected_cards: list = []
number_of_selected_card: int = -1
name: str
role: str

print(' - - - - - - - - - - - / ♠ / ♥ / ♦ / ♣ / - - - - - - - - - - -')
while play_game not in ['oui','non']:
    play_game = str(input('Voulez-vous jouer au jeu de carte : Le Président ? (oui ou non) ').lower())
    if play_game == 'non':
        print('A bientôt')
        print(' - - - - - - - - - - - / ♠ / ♥ / ♦ / ♣ / - - - - - - - - - - -')
        exit()
    elif play_game == 'oui':
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')
        print('Le Jeu du Président peut recevoir de 3 à 6 joueurs.')
        print('Pour pourrez ajouter des IA pour faire une partie à 6 joueurs maximum.')
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')

        while nb_players < NB_PLAYERS_MIN or nb_players > NB_PLAYERS_MAX:
            nb_players = int(input('Combien de joueurs êtes vous pour la partie ? (min : 3 à max : 6) '))
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')
        print('La partie sera constituée de ' + str(nb_players) + ' joueurs.')
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')

        while nb_players_human <= 0 or nb_players_human > nb_players:
            nb_players_human = int(input('Combien d\'humains êtes vous pour la partie ? '))
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')
        print('Enregistrement de vos noms : ')
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')
        i = 1
        while i <= nb_players_human:
            name = str(input('Veuillez rentrer le pseudo du joueur ' + str(i) + ' svp ? '))
            players_name.append(models.Player(name))
            # models.Player.role = 'human'
            i += 1
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')
        nb_players_ia = nb_players - nb_players_human
        i = 1
        while i <= nb_players_ia:
            players_name.append(models.Player())
            # models.Player.role = 'ia'
            i += 1
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')
        # Display list of players (humans and IA)
        print('Les joueurs enregistrés sont : ')
        for i in range(len(players_name)):
            print('- Joueur ' + str(i + 1) + ' : ' + players_name[i].name )
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')

        # 1ST ROUND to determine the President and the Asshole
        print('Début de la partie !')
        print('Dame de coeur à vous l\'honneur !')
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')

        game = models.PresidentGame()
        game.add_players(players_name)
        game.distribute_cards()
        game.sort_hand_players()
        start_player = game.player_have_the_queen_of_hearth()
        index = 0

        start_player_tour = game.players[0]

        print(start_player.name, "à vous de commencer ! ")
        print(start_player.hand)
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')

        # First game = No President or Asshole
        # while president != '' and asshole!='':
        # 1er coup
        table_of_game = []
        nb_card_remove = 0

        # First player is a human, he must choose his cards
        # if start_player.role == 'human':
        print('Combien de cartes (1 à 4) voulez-vous sélectionner ?')
        print('Pour passer écrire \' 0 \' !')

        while number_of_selected_card < 0 or number_of_selected_card > 4:
            number_of_selected_card = int(input('Passer ou poser ? '))

            if number_of_selected_card == 0:
                next_player = game.next_player()

            else:
                print('Veuillez choisir vos cartes ? ')
                i = 1
                while i <= number_of_selected_card:
                    choice = input('Choix de la carte ' + str(i) + ' : ')
                    selected_cards.append(choice)
                    i += 1

        # Check that the 1 to 4 cards are identical
        # and return the card number to remove

        # ELSE IF THE PLAYER IS AN AI THEN
        # if start_player.role == 'ia':
        # function player_card_chosen_by_ia()

        # then, we display the game table with the new cards placed
        # and remove the cards from the player's hand

        # table_of_game.extend(start_player.hand[0:nb_card_remove])
        # del start_player.hand[0:nb_card_remove]

        # Add selected cards to table 0f game
        table_of_game.append(selected_cards)

        print("table de jeu : ", table_of_game)
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')

        # First round and 2nd shot
        next_player = game.next_player()
        print(next_player.name, "à vous de jouer ! ")
        print(next_player.hand)
        print(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')
